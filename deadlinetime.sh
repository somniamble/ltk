#!/bin/sh

# outputs the current deadline-time, in hh:mm:dd format.
# deadline time is decimal, starting at epoch time 0

# yettuktime is number of seconds since 1 January 00:00 UTC (Year 2000)
YETTUK="946684800"
epoch="$(date +%s)"
yettuktime=$((epoch - YETTUK))

# 2 right-most digits           . Deadline seconds.
s=$((yettuktime % 100))

# 2 left-of-right-most-2 digits . Deadline minutes.
m=$((((yettuktime % 10000)) / 100))

# 2 left-of-right-most-42 digits. Deadline hours.
h=$((((yettuktime % 1000000)) / 10000))

# left-padded 2-digit timestamp
printf "%02d:%02d:%02d\n" $h $m $s
